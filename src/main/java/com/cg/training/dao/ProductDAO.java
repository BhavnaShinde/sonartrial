package com.cg.training.dao;

import java.sql.SQLException;
import java.util.List;

import com.cg.training.bean.Product;

public interface ProductDAO {
	public Integer addProduct(Product product) throws SQLException;
	public Product getProductById(Integer productId) throws SQLException;
	public Integer deleteProduct(Integer productId) throws SQLException;
	public List<Product> getAllProducts() throws SQLException;
	public Product updateProduct(Product product) throws SQLException;
	
}
