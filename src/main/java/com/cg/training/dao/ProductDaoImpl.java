package com.cg.training.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cg.training.bean.Product;
import com.cg.training.util.PostgresConnection;

public class ProductDaoImpl implements ProductDAO{

	public Integer addProduct(Product product) throws SQLException {
		try(
			Connection connection=
									PostgresConnection.getConnection();	
			PreparedStatement preparedStatement=
					connection.prepareStatement(QueryMapper.ADD_NEW_PRODUCT);	
				){
			preparedStatement.setInt(1, product.getProductId());
			preparedStatement.setString(2, product.getProductName());
			preparedStatement.setString(3, product.getProductCategory());
			//convert java.time.LocalDate to java.sql.Date			
			preparedStatement
				.setDate(4,java.sql.Date.valueOf(product.getManufactureDate()));
			preparedStatement.setDate(5,
					java.sql.Date.valueOf(product.getExpiryDate()));
			preparedStatement.setDouble(6, product.getProductPrice());
			int n= preparedStatement.executeUpdate();
			if(n==1) {
				return product.getProductId();
			}else {
				throw new SQLException("Unable to add product");
			}			
			
		}catch(SQLException e) {
			e.printStackTrace();
			throw e;
		}
		
	}

	public Product getProductById(Integer productId) throws SQLException {
		try(
			Connection connection= PostgresConnection.getConnection();
			PreparedStatement preparedStatement=
					connection.prepareStatement(QueryMapper.GET_PRODUCT_BY_ID);
				){
			preparedStatement.setInt(1, productId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				Product product= new Product();
				populateProduct(resultSet, product);
				return product;
			}else {
				throw new SQLException("No product with the given id");
			}			
			
		}catch(SQLException e) {
			e.printStackTrace();
			throw e;
		}
		
	}

	private void populateProduct(ResultSet resultSet, Product product) throws SQLException {
		product.setProductId(resultSet.getInt("product_id"));
		product.setProductName(resultSet.getString("product_name"));
		product.setProductCategory(resultSet.getString("product_catagory"));
		//convert java.sql.Date to java.time.LocalDate
		product
		.setManufactureDate(resultSet.getDate("manufacture_date").toLocalDate());
		product
		.setExpiryDate(resultSet.getDate("expiry_date").toLocalDate());
		product.setProductPrice(resultSet.getDouble("product_price"));
	}

	public Integer deleteProduct(Integer productId) throws SQLException {
		try(
				Connection connection=
										PostgresConnection.getConnection();	
				PreparedStatement preparedStatement=
						connection.prepareStatement(QueryMapper.DELETE_PRODUCT);	
					){
				preparedStatement.setInt(1, productId);				
				int n= preparedStatement.executeUpdate();
				if(n==1) {
					return productId;
				}else {
					throw new SQLException("Invalid productId");
				}				
				
			}catch(SQLException e) {
				e.printStackTrace();
				throw e;
			}
	}

	public List<Product> getAllProducts() throws SQLException {
		try(
				Connection connection= PostgresConnection.getConnection();
				Statement statement=connection.createStatement();
				){
			ResultSet resultSet= 
					statement.executeQuery(QueryMapper.GET_ALL_PRODUCTS);
			List<Product> productList= new ArrayList<>();
			while(resultSet.next()) {
				Product product=new Product();
				populateProduct(resultSet,product);
				productList.add(product);
			}
			if(productList.size()!=0) {
				return productList;		
			}else{
				throw new SQLException("No rows in the table");
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
			throw e;
		}
		
	}

	public Product updateProduct(Product product) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
