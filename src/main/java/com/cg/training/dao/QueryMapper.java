package com.cg.training.dao;

public interface QueryMapper {
	public static final String ADD_NEW_PRODUCT="insert into product values(?,?,?,?,?,?)";
	public static final String GET_PRODUCT_BY_ID="select * from product where product_id=?";
	public static final String UPDATE_PRODUCT="update product "
			+ " set product_id=?, product_name=?, "
			+ "product_catagory=?, manufacture_date=?,"
			+ " expiry_date=?, product_price=? where product_id=?";
	public static final String DELETE_PRODUCT="delete from product where product_id=?";
	public static final String GET_ALL_PRODUCTS="select * from product";
}