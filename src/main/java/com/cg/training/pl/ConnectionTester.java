package com.cg.training.pl;

import java.sql.Connection;
import java.sql.SQLException;

import com.cg.training.util.PostgresConnection;

public class ConnectionTester {

	public static void main(String[] args) {
		try {
			Connection connection=
					PostgresConnection.getConnection();
			if(connection != null) {
				System.out.println("Connected to postgreSQL");
			}else {
				System.out.println("Unable to connect");
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

}
