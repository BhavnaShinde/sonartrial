package com.cg.training.pl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import com.cg.training.bean.Product;
import com.cg.training.exception.ProductException;
import com.cg.training.service.ProductService;
import com.cg.training.service.ProductServiceImpl;

public class ProductCRUDTester {
	private static Scanner scanner=new Scanner(System.in);
	private static ProductService service= new ProductServiceImpl();
	
	public static void main(String[] args) {
		while(true) {
			System.out.println("Enter 1. Add product 2. List product by Id 3. Delete product 4. List all products");
			int option= Integer.parseInt(scanner.nextLine());
			try {
				switch(option) {
				case 1: Product product=new Product();
						Integer productId=addProduct(product);
						System.out.println("Added product: "+productId);
						break;
				case 2: System.out.println("Enter product Id");
						productId = Integer.parseInt(scanner.nextLine());
						product= getProductById(productId);						
						System.out.println(product);
						break;
				case 3: System.out.println("Enter product id: ");
						productId= Integer.parseInt(scanner.nextLine());
						int id= deleteProduct(productId);
						System.out.println("Product: "+id+" deleted");
						break;
				case 4: List<Product> productList= getAllProducts();
						productList.stream().forEach(System.out::println);
						break;
				default: System.out.println("Invalid option");
						 break;				
				
				}
				
				
			}catch(ProductException e) {
				e.printStackTrace();
			}
			
			System.out.println("Enter y to continue..");
			String ch=scanner.nextLine();
			if(!ch.equalsIgnoreCase("y")) {
				break;
			}
			
			
		}

	}

	private static List<Product> getAllProducts() throws ProductException {
		return service.getAllProducts();
	}

	

	private static int deleteProduct(Integer productId) throws ProductException {
		return service.deleteProduct(productId);
	}

	private static Product getProductById(Integer productId) throws ProductException{
		return service.getProductById(productId);		
	}

	private static Integer addProduct(Product product) throws ProductException{
		System.out.println("Enter product Id");
		product.setProductId(Integer.parseInt(scanner.nextLine()));
		System.out.println("Enter product name");
		product.setProductName(scanner.nextLine());
		System.out.println("Enter product category");
		product.setProductCategory(scanner.nextLine());
		//convert String to LocalDate
		System.out.println("Enter product manufacture date(dd/mm/yyyy): ");
		String strDate=scanner.nextLine();
		DateTimeFormatter formatter= DateTimeFormatter.ofPattern("dd/MM/yyyy");		
		product.setManufactureDate(LocalDate.parse(strDate, formatter));
		System.out.println("Enter product expiry date(dd/mm/yyyy): ");
		String strExpiry=scanner.nextLine();
		product.setExpiryDate(LocalDate.parse(strExpiry,formatter));
		System.out.println("Enter product price");
		product.setProductPrice(Double.parseDouble(scanner.nextLine()));
		Integer id= service.addProduct(product);
		return id;
	}

}
