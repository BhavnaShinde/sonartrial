package com.cg.training.service;

import java.sql.SQLException;
import java.util.List;

import com.cg.training.bean.Product;
import com.cg.training.dao.ProductDAO;
import com.cg.training.dao.ProductDaoImpl;
import com.cg.training.exception.ProductException;

public class ProductServiceImpl implements ProductService{
	private ProductDAO productDao= new ProductDaoImpl();

	@Override
	public Integer addProduct(Product product) throws ProductException {
		try {
			//validate product
			Integer productId= productDao.addProduct(product);
			return productId;
		}catch(SQLException e) {
			throw new ProductException(e.getMessage(),e);
		}

	}

	@Override
	public Product getProductById(Integer productId) throws ProductException {
		try {			
			Product product= productDao.getProductById(productId);
			return product;
		}catch(SQLException e) {
			throw new ProductException(e.getMessage(),e);
		}
	}

	@Override
	public Integer deleteProduct(Integer productId) throws ProductException {
		try {			
			Integer id= productDao.deleteProduct(productId);
			return id;
		}catch(SQLException e) {
			throw new ProductException(e.getMessage(),e);
		}
	}

	@Override
	public List<Product> getAllProducts() throws ProductException {
		try {			
			List<Product> productList= 
					productDao.getAllProducts();
			return productList;
		}catch(SQLException e) {
			throw new ProductException(e.getMessage(),e);
		}
	}

	@Override
	public Product updateProduct(Product product) throws ProductException {
		// TODO Auto-generated method stub
		return null;
	}

}
